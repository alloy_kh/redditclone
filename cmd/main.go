package main

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/alloy_kh/redditclone/config"
	authDelivery "gitlab.com/alloy_kh/redditclone/pkg/auth/delivery"
	authRepository "gitlab.com/alloy_kh/redditclone/pkg/auth/repository"
	authUseCase "gitlab.com/alloy_kh/redditclone/pkg/auth/usecase"
	"gitlab.com/alloy_kh/redditclone/pkg/db"
	"gitlab.com/alloy_kh/redditclone/pkg/middleware"
	postsDelivery "gitlab.com/alloy_kh/redditclone/pkg/posts/delivery"
	postsRepo "gitlab.com/alloy_kh/redditclone/pkg/posts/repository"
	postsUseCase "gitlab.com/alloy_kh/redditclone/pkg/posts/usecase"

	sessRepository "gitlab.com/alloy_kh/redditclone/pkg/session/repository"
	sessionUC "gitlab.com/alloy_kh/redditclone/pkg/session/usecase"

	commentsDelivery "gitlab.com/alloy_kh/redditclone/pkg/comments/delivery"
	commentsRepo "gitlab.com/alloy_kh/redditclone/pkg/comments/repository"
	commentsUC "gitlab.com/alloy_kh/redditclone/pkg/comments/usecase"

	"gitlab.com/alloy_kh/redditclone/pkg/utils"
	"go.uber.org/zap"
	"log"
	"net/http"
	"os"
	"time"
)

func initLogger() *zap.SugaredLogger {
	zapLogger, _ := zap.NewProduction()
	return zapLogger.Sugar()
}
func main() {

	log.Println("Starting reddit clone app server....")
	configPath := utils.GetConfigPath(os.Getenv("config"))
	cfgFile, err := config.LoadConfig(configPath, ".")

	if err != nil {
		log.Fatalf("LoadConfig: %v", err)
	}

	cfg, err := config.ParseConfig(cfgFile)

	if err != nil {
		log.Fatalf("ParseConfig: %v", err)
	}

	logger := initLogger()

	defer logger.Sync()

	//setup postgres db
	dbHelper := db.PostgresConnectionHelper{
		Ctx:    context.Background(),
		Logger: logger.Desugar(),
		Cfg:    cfg,
	}
	dbConn, err := dbHelper.NewPsqlConn()
	if err != nil {
		logger.Errorf("database err: %s", err.Error())
		return
	}
	logger.Infow("Database stats",
		"max connections: ", dbConn.Stat().MaxConns(),
		"idle connections: ", dbConn.Stat().IdleConns(),
	)
	defer dbConn.Close()

	//setup redis client
	redisClientHelper := db.RedisConnectionHelper{
		Ctx: context.Background(),
		Cfg: cfg,
	}

	redisClient, err := redisClientHelper.NewRedisConn()

	if err != nil {
		logger.Errorf("redis client err: %s", err.Error())
		return
	}

	logger.Infow("Redis client stats",
		"idle connections: ", redisClient.PoolStats().IdleConns,
		"total connections: ", redisClient.PoolStats().TotalConns,
	)

	//setup mongo db
	mongoClient, err := db.NewMongoConnection(context.Background(), cfg)
	if err != nil {
		logger.Errorf("mongo db client err: %s", err.Error())
		return
	}

	logger.Info("Mongo client running...")

	defer func(ctx context.Context) {
		if err = mongoClient.Disconnect(ctx); err != nil {
			log.Fatalf("error while disconnecting from mongo db %s\n", err.Error())
		}
	}(context.Background())

	//open router
	gMux := mux.NewRouter() // also gorilla mux

	authRepo := authRepository.NewRepository(dbConn)
	sessRedisRepo := sessRepository.NewSessionRedisRepo(redisClient)
	sessRepo := sessRepository.NewSessionRepository(dbConn)
	postsMongoRepo := postsRepo.NewPostsMongoRepository(mongoClient, cfg)
	cmnRepo := commentsRepo.NewPostsMongoRepository(mongoClient, cfg)

	sessUC := sessionUC.NewSessionManager(sessRedisRepo, sessRepo, logger, cfg)
	authUC := authUseCase.NewAuthUseCase(authRepo, sessUC)
	postUC := postsUseCase.NewPostsUseCase(postsMongoRepo)
	cmnUC := commentsUC.NewCommentsUC(cmnRepo)

	authHandlers := authDelivery.NewUserHandlers(authUC, logger)
	postHandlers := postsDelivery.NewPostHandlers(logger, postUC)
	commentsHandlers := commentsDelivery.NewCommentHandlers(logger, cmnUC)

	middlewareManager := middleware.NewMiddlewareManager(logger, sessUC)

	authDelivery.MapAuthRoutes(gMux, authHandlers, middlewareManager)
	postsDelivery.MapPostsRoutes(gMux, postHandlers, middlewareManager)
	commentsDelivery.MapCommentsRoutes(gMux, commentsHandlers, middlewareManager)

	gMux.Use(middlewareManager.LoggerMiddleware)
	gMux.Use(middlewareManager.PanicMiddleware)

	srv := &http.Server{
		Handler: gMux,
		Addr:    ":8081",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
