package config

import (
	"errors"
	"github.com/spf13/viper"
	"log"
	"time"
)

type Config struct {
	Server   ServerConfig
	Postgres PostgresConfig
	Redis    RedisConfig
	Mongo    MongoConfig
}

type ServerConfig struct {
	AppVersion   string
	Port         string
	Mode         string
	JwtSecretKey string
	CookieName   string
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
	SSL          bool
	Debug        bool
}

type PostgresConfig struct {
	PostgresqlHost     string
	PostgresqlPort     string
	PostgresqlUser     string
	PostgresqlPassword string
	PostgresqlDbname   string
	PostgresqlSSLMode  bool
}

// Redis config
type RedisConfig struct {
	RedisAddr      string
	RedisPassword  string
	RedisDB        string
	RedisDefaultDb string
	MinIdleConns   int
	PoolSize       int
	PoolTimeout    int
	Password       string
	DB             int
}

//Mongo config

type MongoConfig struct {
	MongoAddr string
	MongoDB   string
}

func LoadConfig(filename string, filepath string) (*viper.Viper, error) {

	v := viper.New()

	v.SetConfigName(filename)
	v.SetConfigType("yaml")
	v.AddConfigPath(filepath)
	v.AutomaticEnv()
	if err := v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Printf("log file not found in the path [%s]", filepath)
			return nil, errors.New("config file not found")
		}
		return nil, err
	}

	return v, nil
}

func ParseConfig(v *viper.Viper) (*Config, error) {
	cnf := new(Config)
	err := v.Unmarshal(cnf)
	if err != nil {
		log.Printf("unable to decode config file, %v", err)
		return nil, err
	}
	return cnf, nil
}
