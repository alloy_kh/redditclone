package middleware

import (
	"context"
	"errors"
	"gitlab.com/alloy_kh/redditclone/pkg/httpErrors"
	"net/http"
	"strings"
)

// do we have to check db every time user sends token? on each request?

func (m *Manager) AuthenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		authHeader := r.Header.Get("Authorization")

		if authHeader == "" {
			httpErrors.WriteError(w, httpErrors.NewUnAuthorizedError(errors.New("token is missing")))
			return
		}

		splits := strings.Split(authHeader, " ")
		if !strings.Contains(authHeader, "Bearer") || len(splits) < 2 {
			httpErrors.WriteError(w, httpErrors.NewUnAuthorizedError(errors.New("token does not exist in the header")))
			return
		}

		token := splits[1]

		//1. validate token and get user id
		//2. set user_id into the context for future usage
		userCtx, err := m.sessUC.VerifyToken(r.Context(), token)

		if err != nil {
			httpErrors.WriteError(w, err)
			return
		}
		ctx := r.Context()
		ctx = context.WithValue(ctx, "user", userCtx)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
