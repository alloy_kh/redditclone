package middleware

import (
	"gitlab.com/alloy_kh/redditclone/pkg/session"
	"go.uber.org/zap"
)

type Manager struct {
	logger *zap.SugaredLogger
	sessUC session.UseCase
}

func NewMiddlewareManager(logger *zap.SugaredLogger, sessUC session.UseCase) *Manager {
	return &Manager{
		logger: logger,
		sessUC: sessUC,
	}
}
