package middleware

import (
	"net/http"
	"time"
)

func (m *Manager) LoggerMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//fmt.Println("Logger middleware")
		start := time.Now()
		handler.ServeHTTP(w, r)
		m.logger.Infow("Incoming request",
			"Method: ", r.Method,
			"Remote address: ", r.RemoteAddr,
			"Url: ", r.URL.Path,
			"Time: ", time.Since(start),
		)
	})
}
