package middleware

import (
	"net/http"
)

func (m *Manager) PanicMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				http.Error(w, "internal server error", http.StatusInternalServerError)
				m.logger.Errorw("Panic",
					"Url: ", r.URL.Path,
					"Method: ", r.Method,
					"Err: ", err,
				)
			}
		}()
		handler.ServeHTTP(w, r)
	})
}
