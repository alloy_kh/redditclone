package repository

const (
	createTableQuery = `create table users
(
    id         bigserial not null
        constraint users_pk
            primary key,
    email      varchar   not null,
    password   varchar   not null,
    created_at timestamp not null,
    updated_at timestamp,
    login_date timestamp not null,
    role       varchar   not null
); 
create unique index users_email_uindex
    on users (email);`

	createUserQuery = `INSERT INTO users (email, password, role, created_at, updated_at, login_date)
						VALUES ($1, $2, $3, now(), now(), now()) 
						RETURNING *`

	selectUserByIdQuery = `SELECT * FROM users where id=$1`

	selectUserByEmailQuery = `SELECT id,email,password,role,created_at,updated_at,login_date FROM users WHERE email=$1`

	createUserWithSessionView = `create view user_sessions as
select u.*,
       (
           select
               array_to_json(array_agg(row_to_json(sessList.*))) as array_to_json
           from (
                    select s.*
                    from sessions s
                    where s.user_id = u.id
                ) sessList ) as sessions
from users u;`

	getUsersWithSessions = `select  row_to_json(row)
from (
         select * from user_sessions
     ) row;`
)
