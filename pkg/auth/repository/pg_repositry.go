package repository

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"gitlab.com/alloy_kh/redditclone/pkg/auth"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
)

type userRepo struct {
	pgConn *pgxpool.Pool
}

func NewRepository(pgConn *pgxpool.Pool) auth.Repository {
	return &userRepo{
		pgConn: pgConn,
	}
}

func (r *userRepo) Create(ctx context.Context, newUser *models.User) (*models.User, error) {

	_, err := r.pgConn.Exec(ctx, createUserQuery, newUser.Email, newUser.Password, newUser.Role)

	if err != nil {
		return nil, errors.Wrap(err, "authRepo.Create.Exec")
	}

	return newUser, nil
}

func (r *userRepo) GetById(ctx context.Context, id int) (*models.User, error) {

	usr := &models.User{}

	err := r.pgConn.
		QueryRow(ctx, selectUserByIdQuery, id).
		Scan(&usr.ID, &usr.Email, &usr.Password)

	if err != nil {
		return nil, errors.Wrap(err, "authRepo.GetById.StructScan")
	}

	return usr, nil
}

func (r *userRepo) GetByEmail(ctx context.Context, email string) (*models.User, error) {

	usr := &models.User{}

	err := r.pgConn.
		QueryRow(ctx, selectUserByEmailQuery, email).
		Scan(&usr.ID, &usr.Email, &usr.Password, &usr.Role, &usr.CreatedAt, &usr.UpdatedAt, &usr.LoginDate)

	if err != nil {
		//return nil, errors.Wrap(err, "authRepo.GetByEmail.StructScan")
		return nil, err
	}

	return usr, nil
}
