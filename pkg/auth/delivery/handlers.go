package delivery

import (
	"encoding/json"
	"gitlab.com/alloy_kh/redditclone/pkg/auth"
	"gitlab.com/alloy_kh/redditclone/pkg/httpErrors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"gitlab.com/alloy_kh/redditclone/pkg/utils"
	"go.uber.org/zap"
	"net/http"
)

type userHandlers struct {
	useCase auth.UseCase
	logger  *zap.SugaredLogger
}

func NewUserHandlers(useCase auth.UseCase, logger *zap.SugaredLogger) auth.Handlers {
	return &userHandlers{
		useCase: useCase,
		logger:  logger,
	}
}

func (u *userHandlers) Login(w http.ResponseWriter, r *http.Request) {

	usr := &models.Login{}

	err := utils.ReadFromRequest(r, usr)

	if err != nil {
		u.logger.Errorf("Login.ReadFromRequest %v", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	justToken, err := u.useCase.Login(r.Context(), &models.User{
		Email:    usr.Email,
		Password: usr.Password,
	})

	if err != nil {
		u.logger.Errorf("error from login: %v", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	res, _ := json.Marshal(justToken)

	w.Write(res)

}

func (u *userHandlers) Register(w http.ResponseWriter, r *http.Request) {

	type Registration struct {
		Email    string `json:"email" valid:"required"`
		Password string `json:"password" valid:"required"`
		Role     string `json:"role" valid:"required"`
	}

	regUser := &Registration{}

	err := utils.ReadFromRequest(r, regUser)

	if err != nil {
		u.logger.Errorf("Register.ReadFromRequest %v", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	// save user to the db
	token, err := u.useCase.Register(r.Context(), &models.User{
		Email:    regUser.Email,
		Password: regUser.Password,
		Role:     regUser.Role,
	})

	if err != nil {
		u.logger.Errorf("Register.Register %v", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	// save
	res, _ := json.Marshal(token)

	w.Write(res)
}
