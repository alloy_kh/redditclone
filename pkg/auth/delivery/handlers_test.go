package delivery

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/alloy_kh/redditclone/pkg/auth/mock"
	"gitlab.com/alloy_kh/redditclone/pkg/httpErrors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

// here we need to test our handlers
func TestUserHandlers_Login(t *testing.T) {

	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockAuthUC := mock.NewMockUseCase(ctrl)

	logger, err := zap.NewProduction()

	require.NoError(t, err)

	handler := NewUserHandlers(mockAuthUC, logger.Sugar())

	login := &models.Login{
		Email:    "email@mail.com",
		Password: "123456",
	}

	js, _ := json.Marshal(login)
	user := &models.User{
		Email:    login.Email,
		Password: login.Password,
	}

	t.Run("invalid body values", func(t *testing.T) {
		httpErr := &httpErrors.RestError{}
		wr := httptest.NewRecorder()
		//passing username instead of email
		rbody := `{
		"username": 123,
		"password": "alloy"
}`
		req := httptest.NewRequest(http.MethodPost, "/api/login", strings.NewReader(rbody))

		handler.Login(wr, req)

		res := wr.Result()
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err)

		err = json.Unmarshal(body, httpErr)

		require.NoError(t, err)
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		require.Equal(t, "invalid values", httpErr.ErrError)

	})

	t.Run("login success", func(t *testing.T) {

		justToken := &models.JustToken{
			Token: "token",
		}

		mockAuthUC.EXPECT().Login(context.Background(), gomock.Eq(user)).Return(justToken, nil)

		req := httptest.NewRequest(http.MethodPost, "/api/login", bytes.NewReader(js))
		wr := httptest.NewRecorder()
		handler.Login(wr, req)

		resp := wr.Result()
		body, _ := ioutil.ReadAll(resp.Body)

		err = json.Unmarshal(body, justToken)

		require.NoError(t, err)
		require.NotNil(t, justToken)

		if !strings.Contains(justToken.Token, "token") {
			t.Errorf("test failed %v", justToken)
		}
	})

	t.Run("login use case err handle", func(t *testing.T) {

		req := httptest.NewRequest(http.MethodPost, "/api/login", bytes.NewReader(js))

		wr := httptest.NewRecorder()

		mockAuthUC.EXPECT().Login(context.Background(), user).Return(nil, httpErrors.NewInternalServerError(nil))

		handler.Login(wr, req)

		res := wr.Result()
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err)
		require.NotNil(t, body)

		errResp := &httpErrors.RestError{}

		err = json.Unmarshal(body, errResp)

		require.NoError(t, err)

		require.Equal(t, http.StatusInternalServerError, errResp.ErrStatus)
		require.Equal(t, "internal server error", errResp.ErrError)

	})

}

func TestUserHandlers_Register(t *testing.T) {

	t.Parallel()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockAuthUC := mock.NewMockUseCase(ctrl)

	logger, err := zap.NewProduction()

	require.NoError(t, err)

	handler := NewUserHandlers(mockAuthUC, logger.Sugar())

	type Registration struct {
		Email    string `json:"email" valid:"required"`
		Password string `json:"password" valid:"required"`
		Role     string `json:"role" valid:"required"`
	}

	regUser := &Registration{
		Email:    "alloy@gmail.com",
		Password: "12345",
		Role:     "user",
	}

	js, _ := json.Marshal(regUser)
	user := &models.User{
		Email:    regUser.Email,
		Password: regUser.Password,
		Role:     regUser.Role,
	}

	t.Run("invalid body", func(t *testing.T) {

		invBody := `{
	"email": 12345,
	"password": "12345",
	"role": "admin"
}`

		req := httptest.NewRequest(http.MethodPost, "/api/register", strings.NewReader(invBody))

		wr := httptest.NewRecorder()

		handler.Register(wr, req)

		res := wr.Result()

		require.Equal(t, http.StatusBadRequest, res.StatusCode)

		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err)

		restErr := &httpErrors.RestError{}

		err = json.Unmarshal(body, restErr)

		require.NoError(t, err)

		require.Equal(t, httpErrors.BadRequest.Error(), restErr.ErrError)
		require.Equal(t, http.StatusBadRequest, restErr.ErrStatus)

	})

	t.Run("register success", func(t *testing.T) {

		wr := httptest.NewRecorder()

		req := httptest.NewRequest(http.MethodPost, "/api/register", bytes.NewReader(js))

		userWithToken := &models.UserWithToken{
			User:  user,
			Token: "token",
		}

		mockAuthUC.EXPECT().Register(context.Background(), user).Return(userWithToken, nil)

		handler.Register(wr, req)

		res := wr.Result()

		require.Equal(t, http.StatusOK, res.StatusCode)

		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err)

		resultJson := &models.UserWithToken{}
		err = json.Unmarshal(body, resultJson)
		require.NoError(t, err)

		require.Equal(t, "token", resultJson.Token)
		require.Equal(t, user.Email, resultJson.User.Email)
		require.Equal(t, user.Role, resultJson.User.Role)

	})

	t.Run("register use case err", func(t *testing.T) {

		httpErr := httpErrors.NewInternalServerError(nil)

		mockAuthUC.EXPECT().Register(context.Background(), user).Return(nil, httpErr)

		wr := httptest.NewRecorder()

		req := httptest.NewRequest(http.MethodPost, "/api/register", bytes.NewReader(js))

		handler.Register(wr, req)

		res := wr.Result()
		require.Equal(t, res.StatusCode, http.StatusInternalServerError)

		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err)

		restErr := &httpErrors.RestError{}

		err = json.Unmarshal(body, restErr)
		require.NoError(t, err)

		require.Equal(t, httpErrors.InternalServerError.Error(), restErr.ErrError)

	})

}
