package delivery

import (
	"github.com/gorilla/mux"
	"gitlab.com/alloy_kh/redditclone/pkg/auth"
	"gitlab.com/alloy_kh/redditclone/pkg/middleware"
	"net/http"
)

func MapAuthRoutes(router *mux.Router, handlers auth.Handlers, mm *middleware.Manager) {
	router.Path("/api/login").HandlerFunc(handlers.Login).Methods("POST")
	router.Path("/api/register").HandlerFunc(handlers.Register).Methods("POST")
	router.Path("/api/secure").Handler(mm.AuthenticationMiddleware(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<h1>Hello secured</h1>`))
	})))
}
