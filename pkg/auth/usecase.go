package auth

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
)

type UseCase interface {
	Login(ctx context.Context, u *models.User) (*models.JustToken, error)
	Register(ctx context.Context, u *models.User) (*models.UserWithToken, error)
}
