package auth

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
)

type Repository interface {
	Create(ctx context.Context, newUser *models.User) (*models.User, error)
	GetById(ctx context.Context, id int) (*models.User, error)
	GetByEmail(ctx context.Context, email string) (*models.User, error)
}
