package usecase

import (
	"context"
	pkgErr "errors"
	"github.com/jackc/pgx"
	pgxv4 "github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"gitlab.com/alloy_kh/redditclone/pkg/auth"
	"gitlab.com/alloy_kh/redditclone/pkg/httpErrors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"gitlab.com/alloy_kh/redditclone/pkg/session"
	"net/http"
	"strings"
)

type authUC struct {
	repo   auth.Repository
	sessUC session.UseCase
}

func NewAuthUseCase(repo auth.Repository, sessUC session.UseCase) auth.UseCase {
	return &authUC{
		repo:   repo,
		sessUC: sessUC,
	}
}

func (uc *authUC) Login(ctx context.Context, u *models.User) (*models.JustToken, error) {

	// if user session does not exist in redis and then	get it from db
	dbUser, err := uc.repo.GetByEmail(ctx, u.Email)
	if err != nil {
		return nil, err
	}

	//	compare password
	if err = dbUser.CheckPassword(u.Password); err != nil {
		return nil, httpErrors.NewRestError(http.StatusUnauthorized, "Invalid username and/or password", nil)
	}
	dbUser.SanitizePassword()

	// call create session and pass user and get user with token
	token, err := uc.sessUC.GenerateToken(ctx, dbUser)
	if err != nil {
		return nil, httpErrors.NewInternalServerError(errors.Wrap(err, "authUC.Login.GenerateToken"))
	}

	return &models.JustToken{Token: token}, nil
}

func (uc *authUC) Register(ctx context.Context, u *models.User) (*models.UserWithToken, error) {

	// check if user exists
	dbUser, err := uc.repo.GetByEmail(ctx, u.Email)

	if err != nil && (!pkgErr.Is(err, pgx.ErrNoRows) || !pkgErr.Is(err, pgxv4.ErrNoRows)) && !strings.Contains(err.Error(), "no rows") {
		return nil, httpErrors.NewInternalServerError(err)
	}

	if dbUser != nil {
		return nil, httpErrors.NewRestError(http.StatusBadRequest, "email already exists", nil)
	}

	if err = u.HashPassword(); err != nil {
		return nil, httpErrors.NewInternalServerError(err.Error())
	}

	// save user to the db
	usr, err := uc.repo.Create(ctx, u)

	u.SanitizePassword()

	if err != nil {
		return nil, httpErrors.NewInternalServerError(err)
	}

	// generate token and return
	token, err := uc.sessUC.GenerateToken(ctx, dbUser)
	if err != nil {
		return nil, httpErrors.NewInternalServerError(errors.Wrap(err, "authUC.Login.GenerateToken"))
	}
	// create session and save it to db and redis
	return &models.UserWithToken{
		User:  usr,
		Token: token,
	}, nil
}
