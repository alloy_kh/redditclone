package db

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/alloy_kh/redditclone/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

//const MongoUri = "mongodb://localhost:27017"
func NewMongoConnection(ctx context.Context, cfc *config.Config) (*mongo.Client, error) {

	clientOps := options.Client()
	clientOps.ApplyURI(cfc.Mongo.MongoAddr)

	client, err := mongo.Connect(ctx, clientOps)

	if err != nil {
		return nil, errors.Wrap(err, "DB.NewMongoConnection.Connect")
	}

	err = client.Ping(ctx, readpref.Primary())

	if err != nil {
		return nil, errors.Wrap(err, "DB.NewMongoConnection.Ping")
	}

	//db := client.Database(cfc.Mongo.MongoDB)
	return client, nil
}
