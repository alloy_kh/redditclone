package db

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/log/zapadapter"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/alloy_kh/redditclone/config"
	"go.uber.org/zap"
)

/**
Created by Alloy
Postgre sql documentation for url construction
https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING
33.1.1.2. Connection URIs
*/

const (
	maxOpenConns    = 60
	connMaxLifetime = 120
	maxIdleConns    = 30
	connMaxIdleTime = 20
)

type PostgresConnectionHelper struct {
	Ctx    context.Context
	Logger *zap.Logger
	Cfg    *config.Config
}

// todo test with sqlx with max and idle connections time and db stats
func (h *PostgresConnectionHelper) NewPsqlConn() (*pgxpool.Pool, error) {

	dataSource := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s",
		h.Cfg.Postgres.PostgresqlHost,
		h.Cfg.Postgres.PostgresqlPort,
		h.Cfg.Postgres.PostgresqlUser,
		h.Cfg.Postgres.PostgresqlDbname,
		h.Cfg.Postgres.PostgresqlPassword,
	)

	parsedCfg, err := pgxpool.ParseConfig(dataSource)

	if err != nil {
		return nil, err
	}

	dbConn, err := pgxpool.ConnectConfig(h.Ctx, parsedCfg)

	if err != nil {
		return nil, err
	}

	dbConn.Config().MaxConns = maxOpenConns
	dbConn.Config().MaxConnLifetime = connMaxLifetime
	dbConn.Config().MinConns = maxIdleConns
	dbConn.Config().MaxConnIdleTime = connMaxIdleTime

	if err = dbConn.Ping(h.Ctx); err != nil {
		return nil, fmt.Errorf("database ping error %s", err.Error())
	}

	dbConn.Config().ConnConfig.Logger = zapadapter.NewLogger(h.Logger)

	return dbConn, nil
}
