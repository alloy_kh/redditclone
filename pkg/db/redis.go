package db

import (
	"context"
	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"gitlab.com/alloy_kh/redditclone/config"
	"time"
)

type RedisConnectionHelper struct {
	Ctx context.Context
	Cfg *config.Config
}

func (r *RedisConnectionHelper) NewRedisConn() (*redis.Client, error) {
	redisHost := r.Cfg.Redis.RedisAddr

	if redisHost == "" {
		redisHost = ":6379"
	}

	client := redis.NewClient(&redis.Options{
		Addr:         r.Cfg.Redis.RedisAddr,
		Password:     r.Cfg.Redis.Password,
		DB:           r.Cfg.Redis.DB,
		MinIdleConns: r.Cfg.Redis.MinIdleConns,
		PoolSize:     r.Cfg.Redis.PoolSize,
		PoolTimeout:  time.Duration(r.Cfg.Redis.PoolTimeout) * time.Second,
	})

	if err := client.Ping(r.Ctx).Err(); err != nil {
		return nil, errors.Wrap(err, "redis.NewRedisConn.Ping")
	}

	return client, nil
}
