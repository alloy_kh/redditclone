package utils

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/rs/xid"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"time"
)

var salt = []byte(`"someKey"`)

type TokeClaims struct {
	UserName string `json:"username"`
	ID       string `json:"id"`
	jwt.StandardClaims
}

func GenerateToken(u *models.User) (string, error) {

	s := &models.Session{
		SessionId: xid.New().String(),
		UserInfo:  u.GetShortUserInfo(),
	}

	tc := TokeClaims{
		UserName: u.Email,
		ID:       s.SessionId,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
			IssuedAt:  time.Now().Unix(),
			Issuer:    "RedditCloneApp",
		},
	}

	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS256, tc)

	token, err := rawToken.SignedString(salt)

	if err != nil {
		return "", fmt.Errorf("error while creating token %v", err)
	}

	// save session into db

	return token, nil
}

func VerifyToken(rawToken string) (string, error) {

	if rawToken == "" {
		return "", fmt.Errorf("token is invalid")
	}

	token, err := jwt.ParseWithClaims(rawToken, &TokeClaims{}, func(token *jwt.Token) (interface{}, error) {
		method, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok || method.Alg() != "HS256" {
			return nil, fmt.Errorf("bad sign method")
		}
		return salt, nil
	})
	// you can return different error messages by parsing error into jwt token errors struct

	if err != nil || !token.Valid {
		return "", fmt.Errorf("invalid token %v", err)
	}

	tc, ok := token.Claims.(*TokeClaims)

	if !ok && !token.Valid {
		return "", fmt.Errorf("%v %v", tc.UserName, tc.StandardClaims.ExpiresAt)
	}

	return tc.UserName, nil
}
