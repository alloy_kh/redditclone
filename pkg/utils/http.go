package utils

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/asaskevich/govalidator"
	"gitlab.com/alloy_kh/redditclone/pkg/httpErrors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"io"
	"net/http"
)

// Get config path for local or docker
func GetConfigPath(configPath string) string {
	if configPath == "docker" {
		return "./config/config-docker"
	}
	return "./config/config-local"
}

func ReadFromRequest(r *http.Request, dto interface{}) error {

	decoder := json.NewDecoder(r.Body)
	//decoder.DisallowUnknownFields()

	err := decoder.Decode(dto)

	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError
		switch {
		case errors.As(err, &syntaxError):
			msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)
			return httpErrors.NewBadRequest(msg)
		case errors.Is(err, io.ErrUnexpectedEOF):
			msg := fmt.Sprintf("Request body contains badly-formed JSON")
			return httpErrors.NewBadRequest(msg)
		case errors.As(err, &unmarshalTypeError):
			msg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", unmarshalTypeError.Field, unmarshalTypeError.Offset)
			return httpErrors.NewBadRequest(msg)
		case errors.Is(err, io.EOF):
			return httpErrors.NewBadRequest(err)
		case err.Error() == "http: request body too large":
			return httpErrors.NewRestError(http.StatusBadRequest, "request body too large", err)
		default:
			return err
		}
	}

	//err = decoder.Decode(&struct{}{})
	//if err != io.EOF {
	//	msg := "Request body must only contain a single JSON object"
	// return error
	//}

	_, err = govalidator.ValidateStruct(dto)

	//fixme return httpErrors.NewValidationErr(...) by setting errors field
	if err != nil {
		if ers, ok := err.(govalidator.Errors); ok {
			causes := make([]string, len(ers))
			for i, er := range ers {
				causes[i] = er.Error()
			}
			return httpErrors.NewRestError(http.StatusBadRequest, "invalid values", causes)
		}
		return httpErrors.NewInternalServerError(err)
	}
	return nil
}

func WriteJsonResponse(w http.ResponseWriter, model interface{}) {
	data, _ := json.Marshal(model)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

func WriteDeletedResponse(w http.ResponseWriter) {
	data, _ := json.Marshal(map[string]string{
		"deleted": "true",
	})
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}

func GetUserFromContext(ctx context.Context) (*models.Session, error) {
	sess, ok := ctx.Value("user").(*models.Session)
	if !ok {
		return nil, httpErrors.UnAuthorized
	}
	return sess, nil
}
