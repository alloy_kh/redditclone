package comments

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UseCase interface {
	Create(ctx context.Context, comment *models.Comment) (*models.Post, error)
	Delete(ctx context.Context, username string, postId, commentId primitive.ObjectID) (*models.Post, error)
}
