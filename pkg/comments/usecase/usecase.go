package usecase

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/comments"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type commentsUC struct {
	repo comments.Repository
}

func NewCommentsUC(repo comments.Repository) comments.UseCase {
	return &commentsUC{repo: repo}
}

func (c *commentsUC) Create(ctx context.Context, comment *models.Comment) (*models.Post, error) {
	p, err := c.repo.Create(ctx, comment)
	if err != nil {
		return nil, err
	}
	return p, nil
}

func (c *commentsUC) Delete(ctx context.Context, username string, postId, commentId primitive.ObjectID) (*models.Post, error) {
	p, err := c.repo.Delete(ctx, username, postId, commentId)
	if err != nil {
		return nil, err
	}
	return p, nil
}
