package repository

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/alloy_kh/redditclone/config"
	"gitlab.com/alloy_kh/redditclone/pkg/comments"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type commentsRepository struct {
	mongoClient *mongo.Client
	cfc         *config.Config
}

const collectionName = "posts"

func NewPostsMongoRepository(mongoClient *mongo.Client, cfc *config.Config) comments.Repository {
	return &commentsRepository{
		cfc:         cfc,
		mongoClient: mongoClient,
	}
}

func (c *commentsRepository) Create(ctx context.Context, newComment *models.Comment) (*models.Post, error) {

	coll := c.getCurrentCollection()

	ops := options.FindOneAndUpdate().SetUpsert(false)
	ops.SetReturnDocument(options.After)
	newComment.ID = primitive.NewObjectID()

	filter := bson.D{{"_id", newComment.PostId}}
	update := bson.M{"$push": bson.M{"comments": newComment}}
	p := &models.Post{}

	err := coll.FindOneAndUpdate(ctx, filter, update, ops).Decode(p)
	if err != nil {
		return nil, errors.Wrap(err, "commentsRepository.Create.FindOneAndUpdate")
	}

	return p, nil
}

func (c *commentsRepository) Delete(ctx context.Context, username string, postId, commentId primitive.ObjectID) (*models.Post, error) {

	coll := c.getCurrentCollection()

	match := bson.M{"_id": postId}
	//filter := bson.M{"$pull": bson.M{"comments": bson.M{"_id": commentId}}}
	filter := bson.M{"$pull": bson.M{"comments": bson.M{"_id": commentId}}}
	p := &models.Post{}

	err := coll.FindOneAndUpdate(ctx, match, filter).Decode(p)

	if err != nil {
		return nil, errors.Wrap(err, "commentsRepository.Delete.FindOneAndDelete")
	}

	return p, nil
}

func (c *commentsRepository) getCurrentCollection() *mongo.Collection {
	return c.mongoClient.Database(c.cfc.Mongo.MongoDB).Collection(collectionName)
}
