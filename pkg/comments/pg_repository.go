package comments

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Repository interface {
	Create(ctx context.Context, newComment *models.Comment) (*models.Post, error)
	Delete(ctx context.Context, username string, postId, commentId primitive.ObjectID) (*models.Post, error)
}
