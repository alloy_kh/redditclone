package delivery

import (
	"github.com/gorilla/mux"
	"gitlab.com/alloy_kh/redditclone/pkg/comments"
	"gitlab.com/alloy_kh/redditclone/pkg/httpErrors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"gitlab.com/alloy_kh/redditclone/pkg/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.uber.org/zap"
	"net/http"
	"time"
)

type commentHandlers struct {
	commentUC comments.UseCase
	logger    *zap.SugaredLogger
}

func NewCommentHandlers(logger *zap.SugaredLogger, commentUC comments.UseCase) comments.Handlers {
	return &commentHandlers{
		commentUC: commentUC,
		logger:    logger,
	}
}

//POST /api/post/{POST_ID} - добавление коммента
func (ch *commentHandlers) Create(w http.ResponseWriter, r *http.Request) {

	sess, err := utils.GetUserFromContext(r.Context())
	if err != nil {
		ch.logger.Errorf("unauthorized call to PostsHandlers.Create [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	postId, ok := mux.Vars(r)["post_id"]
	if !ok {
		ch.logger.Error("id variable is required [post]")
		httpErrors.WriteError(w, httpErrors.BadRequest)
		return
	}

	_id, err := primitive.ObjectIDFromHex(postId)
	if err != nil {
		ch.logger.Errorf("cannot parse id [%s]", postId)
		httpErrors.WriteError(w, httpErrors.NewBadRequest(err))
		return
	}

	type CommentCreate struct {
		Body string `json:"body" valid:"required"`
	}

	dto := &CommentCreate{}

	err = utils.ReadFromRequest(r, dto)
	if err != nil {
		ch.logger.Errorf("error while reading from request body [commentHandlers.Create.ReadFromRequest] %s", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	post, err := ch.commentUC.Create(r.Context(), &models.Comment{
		Author:    sess.UserInfo,
		PostId:    _id,
		CreatedAt: time.Now(),
		Body:      dto.Body,
	})

	if err != nil {
		ch.logger.Errorf("error while creating a comment %s", err.Error())
		httpErrors.WriteError(w, err)
		return
	}
	utils.WriteJsonResponse(w, post)
}

//DELETE /api/post/{POST_ID}/{COMMENT_ID} - удаление коммента
func (ch *commentHandlers) Delete(w http.ResponseWriter, r *http.Request) {

	sess, err := utils.GetUserFromContext(r.Context())
	if err != nil {
		ch.logger.Errorf("unauthorized call to PostsHandlers.Create [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	postVarId, ok1 := mux.Vars(r)["post_id"]
	commentVarId, ok2 := mux.Vars(r)["comment_id"]
	if !ok1 || !ok2 {
		ch.logger.Error("id variable is required [post]")
		httpErrors.WriteError(w, httpErrors.NewBadRequest(err))
		return
	}

	postId, err := primitive.ObjectIDFromHex(postVarId)
	if err != nil {
		ch.logger.Errorf("cannot parse post id [%s]", postVarId)
		httpErrors.WriteError(w, httpErrors.NewBadRequest(err))
		return
	}

	commentId, err := primitive.ObjectIDFromHex(commentVarId)
	if err != nil {
		ch.logger.Errorf("cannot parse comment id [%s]", commentVarId)
		httpErrors.WriteError(w, httpErrors.NewBadRequest(err))
		return
	}

	_, err = ch.commentUC.Delete(r.Context(), sess.UserInfo.Username, postId, commentId)

	if err != nil {
		ch.logger.Errorf("error while deleting comment [%s]: %s", commentVarId, err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	utils.WriteDeletedResponse(w)
}
