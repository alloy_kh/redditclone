package delivery

import (
	"github.com/gorilla/mux"
	"gitlab.com/alloy_kh/redditclone/pkg/comments"
	"gitlab.com/alloy_kh/redditclone/pkg/middleware"
)

// [/post] expected subRouter
func MapCommentsRoutes(router *mux.Router, handlers comments.Handlers, middlewares *middleware.Manager) {

	secureRouter := router.PathPrefix("/api/").Subrouter()
	secureRouter.Use(middlewares.AuthenticationMiddleware)
	secureRouter.Path("/post/{post_id}").HandlerFunc(handlers.Create).Methods("POST")
	secureRouter.Path("/post/{post_id}/{comment_id}").HandlerFunc(handlers.Delete).Methods("DELETE")
}
