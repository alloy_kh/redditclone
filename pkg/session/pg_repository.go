package session

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
)

type Repository interface {
	CreateSession(ctx context.Context, sess *models.Session) (*models.Session, error)
	GetSessionById(ctx context.Context, sessionId string) (*models.Session, error)
}
