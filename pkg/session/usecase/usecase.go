package usecase

import (
	"context"
	pkgErr "errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"github.com/rs/xid"
	"gitlab.com/alloy_kh/redditclone/config"
	"gitlab.com/alloy_kh/redditclone/pkg/httpErrors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"gitlab.com/alloy_kh/redditclone/pkg/session"
	"go.uber.org/zap"
	"time"
)

type sessionUseCase struct {
	redisRepository session.RedisRepository
	sessRepo        session.Repository
	conf            *config.Config
	logger          *zap.SugaredLogger
}

type TokeClaims struct {
	UserName string `json:"username"`
	ID       string `json:"id"`
	jwt.StandardClaims
}

func NewSessionManager(redisRepo session.RedisRepository, sessRepo session.Repository, logger *zap.SugaredLogger, conf *config.Config) *sessionUseCase {
	return &sessionUseCase{
		redisRepository: redisRepo,
		sessRepo:        sessRepo,
		conf:            conf,
		logger:          logger,
	}
}

//fixme create tx and rollback if u need

func (m *sessionUseCase) GenerateToken(ctx context.Context, u *models.User) (string, error) {

	sess := &models.Session{
		SessionId: xid.New().String(),
		UserInfo:  u.GetShortUserInfo(),
		CreatedAt: time.Now(),
		Role:      u.Role,
	}

	tc := TokeClaims{
		UserName: u.Email,
		ID:       sess.SessionId,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
			IssuedAt:  time.Now().Unix(),
			Issuer:    "RedditCloneApp",
		},
	}

	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS256, tc)

	token, err := rawToken.SignedString([]byte(m.conf.Server.JwtSecretKey))

	if err != nil {
		return "", errors.Wrap(err, "sessManager.GenerateToken.SignedString")
	}

	sess, err = m.sessRepo.CreateSession(ctx, sess)
	if err != nil {
		return "", err
	}

	err = m.redisRepository.SetSession(ctx, sess)

	if err != nil {
		m.logger.Errorf("error while setting sess into redis %v", err.Error())
	}

	return token, nil
}

func (m *sessionUseCase) VerifyToken(ctx context.Context, rawToken string) (*models.Session, error) {

	if rawToken == "" {
		return nil, httpErrors.NotValidJwt
	}

	token, err := jwt.ParseWithClaims(rawToken, &TokeClaims{}, func(token *jwt.Token) (interface{}, error) {
		method, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok || method.Alg() != "HS256" {
			return nil, httpErrors.NotValidJwt
		}
		return []byte(m.conf.Server.JwtSecretKey), nil
	})

	if err != nil {
		m.logger.Errorf("error while parsing the token %v\n", err.Error())
		return nil, err
	}

	if !token.Valid {
		m.logger.Error("invalid token")
		return nil, httpErrors.NotValidJwt
	}

	tc, ok := token.Claims.(*TokeClaims)

	if !ok {
		m.logger.Errorf("%v %v", tc.UserName, tc.StandardClaims.ExpiresAt)
		return nil, httpErrors.InvalidClaims
	}

	// get session from redis
	sess, err := m.redisRepository.GetSession(ctx, tc.ID)

	if err != nil && pkgErr.Is(err, redis.Nil) {
		m.logger.Errorf("sessMan.VerifyToken.RedisRepo->GetSession %v", err.Error())
	}

	if sess != nil {
		return sess, nil
	}

	// if not exist get it from db and set it into redis
	sess, err = m.sessRepo.GetSessionById(ctx, tc.ID)

	if err != nil {
		return nil, err
	}

	err = m.redisRepository.SetSession(ctx, sess)
	if err != nil {
		m.logger.Errorf("sessMan.VerifyToken.RedisRepo->SetSession %v", err.Error())
	}

	return sess, nil
}
