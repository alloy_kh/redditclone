package session

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
)

type RedisRepository interface {
	SetSession(ctx context.Context, sess *models.Session) error
	GetSession(ctx context.Context, sessionId string) (*models.Session, error)
}
