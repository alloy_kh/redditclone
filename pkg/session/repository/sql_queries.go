package repository

const (
	createSessionQuery = `INSERT INTO sessions (session_id, user_id, created_at) VALUES($1, $2, now())`

	getSessionById = `SELECT s.session_id,s.user_id,u.email,u.role, s.created_at FROM sessions s INNER JOIN users u on u.id = s.user_id WHERE s.session_id=$1`

	createTableQuery = `create table sessions
(
    session_id varchar   not null
        constraint sessions_pk
            primary key,
    user_id    integer   not null
        constraint sessions_users_fk
            references users
            on update restrict on delete restrict,
    created_at timestamp not null
); 
create unique index sessions_session_id_uindex
    on sessions (session_id);`
)
