package repository

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"time"
)

const (
	expiration = 3
)

type sessionRedisRepo struct {
	client *redis.Client
}

func NewSessionRedisRepo(client *redis.Client) *sessionRedisRepo {
	return &sessionRedisRepo{client: client}
}

func (s *sessionRedisRepo) SetSession(ctx context.Context, sess *models.Session) error {

	data, err := json.Marshal(sess)

	if err != nil {
		return errors.Wrap(err, "sessionRedisRepo.SetSession.SessMarshal")
	}

	_, err = s.client.Set(ctx, sess.SessionId, data, expiration*time.Minute).Result()
	if err != nil {
		return errors.Wrap(err, "sessionRedisRepo.SetSession.Set")
	}
	return nil

}

// should there be something to prolong the duration of the session in redis?
func (s *sessionRedisRepo) GetSession(ctx context.Context, sessionId string) (*models.Session, error) {

	data, err := s.client.Get(ctx, sessionId).Bytes()

	if err != nil {
		return nil, errors.Wrap(err, "sessionRedisRepo.GetSession.Get")
	}

	sess := &models.Session{}

	err = json.Unmarshal(data, sess)
	if err != nil {
		return nil, errors.Wrap(err, "sessionRedisRepo.SetSession.UnMarshal")
	}

	return sess, nil
}
