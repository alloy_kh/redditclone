package repository

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"time"
)

type sessionRepo struct {
	pgConn *pgxpool.Pool
}

func NewSessionRepository(pgConn *pgxpool.Pool) *sessionRepo {
	return &sessionRepo{pgConn: pgConn}
}

func (s *sessionRepo) CreateSession(ctx context.Context, sess *models.Session) (*models.Session, error) {
	_, err := s.pgConn.Exec(ctx, createSessionQuery, sess.SessionId, sess.UserInfo.ID)
	if err != nil {
		return nil, errors.Wrap(err, "session.CreateSession.Exec")
	}
	return sess, nil
}

func (s *sessionRepo) GetSessionById(ctx context.Context, sessionId string) (*models.Session, error) {

	time.Sleep(time.Second * 5)
	sess := &models.Session{
		UserInfo: &models.UserShortInfo{},
	}
	err := s.pgConn.QueryRow(ctx, getSessionById, sessionId).Scan(&sess.SessionId, &sess.UserInfo.ID, &sess.UserInfo.Username, &sess.Role, &sess.CreatedAt)

	if err != nil {
		return nil, errors.Wrap(err, "session.GetSessionById.QueryRow")
	}
	return sess, nil
}
