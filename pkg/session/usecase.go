package session

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
)

type UseCase interface {
	GenerateToken(ctx context.Context, u *models.User) (string, error)
	VerifyToken(ctx context.Context, rawToken string) (*models.Session, error)
}
