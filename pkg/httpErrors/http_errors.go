package httpErrors

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"strings"
)

// Rest error struct
type RestError struct {
	ErrStatus int             `json:"status,omitempty"`
	ErrError  string          `json:"error,omitempty"`
	ErrCauses interface{}     `json:"-"`
	Errors    []ValidationErr `json:"errors,omitempty"`
}

type ValidationErr struct {
	Location string
	Param    string
	Value    string
	Msg      string
}

func (err *RestError) Error() string {
	return fmt.Sprintf("status: %d - errors: %s - causes: %v", err.ErrStatus, err.ErrError, err.ErrCauses)
}

func (err *RestError) Marshal() []byte {
	res, _ := json.Marshal(err)
	return res
}

var (
	BadRequest          = errors.New("bad request")
	UnAuthorized        = errors.New("unauthorized")
	InternalServerError = errors.New("internal server error")
	NotFound            = errors.New("not found")
	NotValidJwt         = errors.New("invalid token")
	InvalidClaims       = errors.New("invalid jwt claims")
)

func ParseErrors(err error) *RestError {

	switch {
	case errors.Is(err, pgx.ErrNoRows):
		return NewRestError(http.StatusNotFound, NotFound.Error(), err)
	case errors.Is(err, mongo.ErrNoDocuments):
		return NewRestError(http.StatusNotFound, NotFound.Error(), err)
	case strings.Contains(err.Error(), "token is expired"):
		return NewUnAuthorizedError(nil)
	case strings.Contains(err.Error(), "signature is invalid"):
		return NewBadRequest(err)
	case errors.Is(err, UnAuthorized):
		return NewUnAuthorizedError(err)
	case errors.Is(err, primitive.ErrInvalidHex):
		return NewBadRequest(err)
	case strings.Contains(err.Error(), "encoding/hex:"):
		return NewBadRequest(err)
	default:
		if e, ok := err.(*RestError); ok {
			return e
		}
		return NewInternalServerError(err)
	}

}

func NewRestError(code int, err string, causes interface{}) *RestError {
	return &RestError{
		ErrStatus: code,
		ErrError:  err,
		ErrCauses: causes,
	}
}

func NewBadRequest(causes interface{}) *RestError {
	return &RestError{
		ErrStatus: http.StatusBadRequest,
		ErrError:  BadRequest.Error(),
		ErrCauses: causes,
	}
}

func NewUnAuthorizedError(causes interface{}) *RestError {
	return &RestError{
		ErrStatus: http.StatusUnauthorized,
		ErrError:  UnAuthorized.Error(),
		ErrCauses: causes,
	}
}

func NewInternalServerError(causes interface{}) *RestError {
	result := &RestError{
		ErrStatus: http.StatusInternalServerError,
		ErrError:  InternalServerError.Error(),
		ErrCauses: causes,
	}
	return result
}

func WriteError(w http.ResponseWriter, err error) {
	rErr := ParseErrors(err)
	w.WriteHeader(rErr.ErrStatus)
	w.Header().Set("content-type", "application/json")
	_, _ = w.Write(rErr.Marshal())
}
