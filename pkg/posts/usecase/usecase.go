package usecase

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	pkgPosts "gitlab.com/alloy_kh/redditclone/pkg/posts"
)

type postsUC struct {
	repository pkgPosts.Repository
}

func NewPostsUseCase(repository pkgPosts.Repository) pkgPosts.UseCase {
	return &postsUC{
		repository: repository,
	}
}

func (uc *postsUC) Create(ctx context.Context, newPost *models.PostCreate) (*models.Post, error) {

	post, err := uc.repository.Create(ctx, newPost)

	if err != nil {
		return nil, err
	}

	return post, nil
}

//todo (ask why this is working in pointer based services (layers like in usecase,repository) from Vasiliy)
//var atomicVal uint64 = 0

func (uc *postsUC) GetById(ctx context.Context, id string) (*models.Post, error) {

	//if id == "6082cfdadbeffe979a315e66" {
	//	time.Sleep(10 * time.Second)
	//}
	//
	//atomic.AddUint64(&atomicVal, 1)
	//
	//fmt.Println(atomicVal)

	p, err := uc.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}
	return p, nil
}

func (uc *postsUC) GetAll(ctx context.Context) ([]models.Post, error) {
	allPosts, err := uc.repository.GetAll(ctx)
	if err != nil {
		return nil, err
	}
	return allPosts, nil
}

func (uc *postsUC) GetByCategory(ctx context.Context, category string) ([]models.Post, error) {

	post, err := uc.repository.GetByCategory(ctx, category)

	if err != nil {
		return nil, err
	}
	return post, nil
}

func (uc *postsUC) Delete(ctx context.Context, postId string, username string) error {
	if err := uc.repository.Delete(ctx, postId, username); err != nil {
		return err
	}
	return nil
}

func (uc *postsUC) PostsByUsername(ctx context.Context, username string) ([]models.Post, error) {
	posts, err := uc.repository.PostsByUsername(ctx, username)
	if err != nil {
		return nil, err
	}
	return posts, nil
}

func (uc *postsUC) Upvote(ctx context.Context, postId string, userId int) (*models.Post, error) {
	p, err := uc.repository.Vote(ctx, postId, userId, 1)
	if err != nil {
		return nil, err
	}
	return p, nil
}

func (uc *postsUC) DownVote(ctx context.Context, postId string, userId int) (*models.Post, error) {
	p, err := uc.repository.Vote(ctx, postId, userId, -1)
	if err != nil {
		return nil, err
	}
	return p, nil
}

func (uc *postsUC) UnVote(ctx context.Context, postId string, userId int) (*models.Post, error) {
	p, err := uc.repository.Vote(ctx, postId, userId, 0)
	if err != nil {
		return nil, err
	}
	return p, nil
}
