package delivery

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"gitlab.com/alloy_kh/redditclone/pkg/httpErrors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"gitlab.com/alloy_kh/redditclone/pkg/posts/mock"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func TestPostHandlers_Create(t *testing.T) {

	t.Parallel()
	ctrl := gomock.NewController(t)
	postMockUC := mock.NewMockUseCase(ctrl)

	logger, err := zap.NewProduction()
	require.NoError(t, err)
	sLogger := logger.Sugar()

	handler := NewPostHandlers(sLogger, postMockUC)

	user := &models.UserShortInfo{
		ID:       1,
		Username: "alloy@gmail.com",
	}

	timeNow := time.Now()
	dtoCreate := &models.PostCreate{
		Title:    "some title",
		Category: "music",
		Type:     "link",
		Url:      "url",
	}

	js, _ := json.Marshal(dtoCreate)

	post := &models.Post{
		Title:     dtoCreate.Title,
		Category:  dtoCreate.Category,
		Type:      dtoCreate.Type,
		Url:       dtoCreate.Url,
		Author:    user,
		CreatedAt: timeNow,
	}

	t.Run("unauthorized", func(t *testing.T) {

		req := httptest.NewRequest(http.MethodPost, "/api/posts/", bytes.NewReader(js))
		wr := httptest.NewRecorder()

		handler.Create(wr, req)

		res := wr.Result()

		require.Equal(t, http.StatusUnauthorized, res.StatusCode)

	})

	ctx := getNewContextWithUserInfo(context.Background())

	t.Run("create success", func(t *testing.T) {

		postMockUC.EXPECT().Create(gomock.Any(), gomock.Any()).Return(post, nil)

		req := httptest.NewRequest(http.MethodPost, "/api/posts/", bytes.NewReader(js))
		wr := httptest.NewRecorder()

		handler.Create(wr, req.WithContext(ctx))

		res := wr.Result()

		require.Equal(t, http.StatusOK, res.StatusCode)

		body, err := ioutil.ReadAll(res.Body)

		require.NoError(t, err)

		resPost := &models.Post{}

		err = json.Unmarshal(body, resPost)
		require.NoError(t, err)
		require.Equal(t, resPost.ID, post.ID)
		require.Equal(t, resPost.Title, post.Title)

	})

	t.Run("create use case fail", func(t *testing.T) {

		postMockUC.EXPECT().Create(gomock.Any(), gomock.Any()).Return(nil, httpErrors.NewInternalServerError(nil))
		wr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/api/posts", bytes.NewReader(js))
		handler.Create(wr, req.WithContext(ctx))
		res := wr.Result()
		require.Equal(t, http.StatusInternalServerError, res.StatusCode)
	})

	t.Run("invalid body values", func(t *testing.T) {

		rBody := `{
	"title": "another 2",
	"type": "link",
	"url": "https://www.youtube.com/watch?v=NJCiPXS3-10"
}`
		req := httptest.NewRequest(http.MethodPost, "/api/posts/", strings.NewReader(rBody))

		wr := httptest.NewRecorder()

		handler.Create(wr, req.WithContext(ctx))

		res := wr.Result()
		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err)
		httpErr := &httpErrors.RestError{}
		err = json.Unmarshal(body, httpErr)

		require.NoError(t, err)
		require.Equal(t, http.StatusBadRequest, res.StatusCode)
		require.Equal(t, "invalid values", httpErr.ErrError)

	})

}

func TestPostHandlers_Delete(t *testing.T) {

	t.Parallel()
	ctrl := gomock.NewController(t)
	postMockUC := mock.NewMockUseCase(ctrl)

	logger, err := zap.NewProduction()
	require.NoError(t, err)
	sLogger := logger.Sugar()

	handler := NewPostHandlers(sLogger, postMockUC)
	ctx := getNewContextWithUserInfo(context.Background())

	t.Run("unauthorized", func(t *testing.T) {
		req := httptest.NewRequest(http.MethodPost, "/api/posts/", bytes.NewReader(nil))
		wr := httptest.NewRecorder()
		handler.Create(wr, req)
		res := wr.Result()
		require.Equal(t, http.StatusUnauthorized, res.StatusCode)
	})

	t.Run("delete success", func(t *testing.T) {

		postMockUC.EXPECT().Delete(gomock.Any(), "123", "alloy@gmail.com").Return(nil)

		wr := httptest.NewRecorder()
		vars := map[string]string{
			"id": "123",
		}

		r, _ := http.NewRequest("POST", "hello/1", nil)
		r = r.WithContext(ctx)
		r = mux.SetURLVars(r, vars)

		handler.Delete(wr, r)

		res := wr.Result()

		require.Equal(t, http.StatusOK, res.StatusCode)

	})

}

func getNewContextWithUserInfo(ctx context.Context) context.Context {
	user := &models.UserShortInfo{
		ID:       1,
		Username: "alloy@gmail.com",
	}

	sess := &models.Session{
		SessionId: "1234",
		UserInfo:  user,
		CreatedAt: time.Now(),
		Role:      "admin",
	}
	ctx = context.WithValue(ctx, "user", sess)

	return ctx
}
