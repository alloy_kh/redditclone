package delivery

import (
	"github.com/gorilla/mux"
	"gitlab.com/alloy_kh/redditclone/pkg/httpErrors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	pkgPosts "gitlab.com/alloy_kh/redditclone/pkg/posts"
	"gitlab.com/alloy_kh/redditclone/pkg/utils"
	"go.uber.org/zap"
	"net/http"
)

type postHandlers struct {
	postsUC pkgPosts.UseCase
	logger  *zap.SugaredLogger
}

func NewPostHandlers(logger *zap.SugaredLogger, postsUC pkgPosts.UseCase) pkgPosts.Handlers {
	return &postHandlers{
		postsUC: postsUC,
		logger:  logger,
	}
}

func (ph *postHandlers) Create(w http.ResponseWriter, r *http.Request) {

	sess, err := utils.GetUserFromContext(r.Context())
	if err != nil {
		ph.logger.Errorf("unauthorized call to PostsHandlers.Create [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	p := &models.PostCreate{}
	err = utils.ReadFromRequest(r, p)

	if err != nil {
		ph.logger.Errorf("error while reading from request body [PostsHandlers.Create.ReadFromRequest] %s", err.Error())
		httpErrors.WriteError(w, err)
		return
	}
	p.PreparePostToSave(sess.UserInfo)
	newPost, err := ph.postsUC.Create(r.Context(), p)

	if err != nil {
		ph.logger.Errorf("PostsUC create call: [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}
	utils.WriteJsonResponse(w, newPost)
}

func (ph *postHandlers) GetById(w http.ResponseWriter, r *http.Request) {

	postId, ok := mux.Vars(r)["id"]
	if !ok {
		ph.logger.Error("id variable is required [post]")
		httpErrors.WriteError(w, httpErrors.BadRequest)
		return
	}

	p, err := ph.postsUC.GetById(r.Context(), postId)

	if err != nil {
		ph.logger.Errorf("error while fetching post by its id [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	utils.WriteJsonResponse(w, p)
}

func (ph *postHandlers) GetAll(w http.ResponseWriter, r *http.Request) {

	posts, err := ph.postsUC.GetAll(r.Context())

	if err != nil {
		ph.logger.Errorf("error while fetching posts data: [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}
	utils.WriteJsonResponse(w, posts)
}

func (ph *postHandlers) GetByCategory(w http.ResponseWriter, r *http.Request) {

	postCategory, ok := mux.Vars(r)["category"]
	if !ok {
		ph.logger.Error("category variable is required [post]")
		httpErrors.WriteError(w, httpErrors.BadRequest)
		return
	}

	posts, err := ph.postsUC.GetByCategory(r.Context(), postCategory)

	if err != nil {
		ph.logger.Errorf("error while getting posts by category: [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	utils.WriteJsonResponse(w, posts)
}

func (ph *postHandlers) Delete(w http.ResponseWriter, r *http.Request) {

	sess, err := utils.GetUserFromContext(r.Context())
	if err != nil {
		ph.logger.Errorf("unauthorized call to PostsHandlers.Create [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	postId, ok := mux.Vars(r)["id"]
	if !ok {
		ph.logger.Error("id variable is required [post]")
		httpErrors.WriteError(w, httpErrors.BadRequest)
		return
	}

	err = ph.postsUC.Delete(r.Context(), postId, sess.UserInfo.Username)

	if err != nil {
		ph.logger.Errorf("error while deleting the post with id [%s]: [%s]", postId, err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	utils.WriteDeletedResponse(w)
}

func (ph *postHandlers) GetUserPosts(w http.ResponseWriter, r *http.Request) {
	username, ok := mux.Vars(r)["username"]
	if !ok {
		ph.logger.Error("username variable is required [post]")
		httpErrors.WriteError(w, httpErrors.BadRequest)
		return
	}

	posts, err := ph.postsUC.PostsByUsername(r.Context(), username)

	if err != nil {
		ph.logger.Errorf("error while retrieving post by username: [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	utils.WriteJsonResponse(w, posts)
}

func (ph *postHandlers) UpVote(w http.ResponseWriter, r *http.Request) {

	sess, err := utils.GetUserFromContext(r.Context())
	if err != nil {
		ph.logger.Errorf("unauthorized call to PostsHandlers.upvote [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	postId, ok := mux.Vars(r)["id"]
	if !ok {
		ph.logger.Error("id variable is required [post]")
		httpErrors.WriteError(w, httpErrors.BadRequest)
		return
	}

	post, err := ph.postsUC.Upvote(r.Context(), postId, sess.UserInfo.ID)

	if err != nil {
		ph.logger.Errorf("error while voting: [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	utils.WriteJsonResponse(w, post)
}

func (ph *postHandlers) DownVote(w http.ResponseWriter, r *http.Request) {
	sess, err := utils.GetUserFromContext(r.Context())
	if err != nil {
		ph.logger.Errorf("unauthorized call to PostsHandlers.downvote [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	postId, ok := mux.Vars(r)["id"]
	if !ok {
		ph.logger.Error("id variable is required [post]")
		httpErrors.WriteError(w, httpErrors.BadRequest)
		return
	}

	post, err := ph.postsUC.DownVote(r.Context(), postId, sess.UserInfo.ID)

	if err != nil {
		ph.logger.Errorf("error while downvoting: [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	utils.WriteJsonResponse(w, post)
}

func (ph *postHandlers) UnVote(w http.ResponseWriter, r *http.Request) {
	sess, err := utils.GetUserFromContext(r.Context())
	if err != nil {
		ph.logger.Errorf("unauthorized call to PostsHandlers.unvote [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	postId, ok := mux.Vars(r)["id"]
	if !ok {
		ph.logger.Error("id variable is required [post]")
		httpErrors.WriteError(w, httpErrors.BadRequest)
		return
	}

	post, err := ph.postsUC.UnVote(r.Context(), postId, sess.UserInfo.ID)

	if err != nil {
		ph.logger.Errorf("error while unvoting: [%s]", err.Error())
		httpErrors.WriteError(w, err)
		return
	}

	utils.WriteJsonResponse(w, post)
}
