package delivery

import (
	"github.com/gorilla/mux"
	"gitlab.com/alloy_kh/redditclone/pkg/middleware"
	"gitlab.com/alloy_kh/redditclone/pkg/posts"
)

func MapPostsRoutes(router *mux.Router, handlers posts.Handlers, mManager *middleware.Manager) {
	router.Path("/api/posts/").HandlerFunc(handlers.GetAll).Methods("GET")
	router.Path("/api/post/{id}").HandlerFunc(handlers.GetById).Methods("GET")
	router.Path("/api/posts/{category}").HandlerFunc(handlers.GetByCategory).Methods("GET")
	router.Path("/api/u/{username}").HandlerFunc(handlers.GetUserPosts).Methods("GET")

	//secure /api
	secureRouter := router.PathPrefix("/api/").Subrouter()
	secureRouter.Use(mManager.AuthenticationMiddleware)
	secureRouter.Path("/posts/").HandlerFunc(handlers.Create).Methods("POST")
	secureRouter.Path("/post/{id}").HandlerFunc(handlers.Delete).Methods("DELETE")

	// could take voting to upper layer if you want
	secureRouter.Path("/post/{id}/upvote").HandlerFunc(handlers.UpVote).Methods("POST")
	secureRouter.Path("/post/{id}/downvote").HandlerFunc(handlers.DownVote).Methods("POST")
	secureRouter.Path("/post/{id}/unvote").HandlerFunc(handlers.UnVote).Methods("POST")

}
