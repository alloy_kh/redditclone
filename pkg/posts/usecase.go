package posts

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
)

type UseCase interface {
	GetById(ctx context.Context, id string) (*models.Post, error)
	GetByCategory(ctx context.Context, category string) ([]models.Post, error)
	GetAll(ctx context.Context) ([]models.Post, error)
	Create(ctx context.Context, newPost *models.PostCreate) (*models.Post, error)
	Delete(ctx context.Context, postId string, username string) error
	PostsByUsername(ctx context.Context, username string) ([]models.Post, error)

	Upvote(ctx context.Context, postId string, userId int) (*models.Post, error)
	DownVote(ctx context.Context, postId string, userId int) (*models.Post, error)
	UnVote(ctx context.Context, postId string, userId int) (*models.Post, error)
}
