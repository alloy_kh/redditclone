package repository

const (
	createPostsView = `create view posts_view as
select p.*, u.id, u.email,
       (
           select
               array_to_json(array_agg(row_to_json(commentList.*))) as array_to_json
           from (
               select c.*
               from comments c
               where c.post_id = p.id) as commentList
           ) as comments,
       (
           select
                  array_to_json(array_agg(row_to_json(voteList.*))) as array_to_json
           from (
               select v.*
               from votes v
               where v.post_id = p.id) as voteList
           ) as votes
from posts p
inner join users u on u.id = p.author_id;`

	getPostListFromPostsView = `select array_to_json(row)
from (
     select * from posts_view
         ) row;`
)
