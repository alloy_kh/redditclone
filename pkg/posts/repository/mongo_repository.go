package repository

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/alloy_kh/redditclone/config"
	"gitlab.com/alloy_kh/redditclone/pkg/httpErrors"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type postsRepository struct {
	mongoClient *mongo.Client
	cfc         *config.Config
}

const collectionName = "posts"

func NewPostsMongoRepository(mongoClient *mongo.Client, cfc *config.Config) *postsRepository {
	return &postsRepository{
		cfc:         cfc,
		mongoClient: mongoClient,
	}
}

func (pr *postsRepository) Create(ctx context.Context, newPost *models.PostCreate) (*models.Post, error) {

	opts := options.Session().SetDefaultReadConcern(readconcern.Majority())
	mongoSession, err := pr.mongoClient.StartSession(opts)

	if err != nil {
		return nil, errors.Wrap(err, "PostMongoRepo.Create.StartSession")
	}

	defer mongoSession.EndSession(ctx)

	txnOpts := options.Transaction().SetReadPreference(readpref.Primary())

	p, err := mongoSession.WithTransaction(ctx, func(sessCtx mongo.SessionContext) (interface{}, error) {

		coll := pr.getCurrentCollection()

		res, err := coll.InsertOne(sessCtx, newPost)

		if err != nil {
			return nil, errors.Wrap(err, "PostsMongoRepo.Create.InsertOne")
		}

		result := &models.Post{}

		if err = coll.FindOne(sessCtx, bson.D{{"_id", res.InsertedID}}).Decode(result); err != nil {
			return nil, err
		}

		return result, err
	}, txnOpts)

	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	result, ok := p.(*models.Post)
	if !ok {
		return nil, errors.New("cannot convert to *post [PostsMongoRepo.Create.p.(type)]")
	}

	return result, nil
}

func (pr *postsRepository) GetById(ctx context.Context, id string) (*models.Post, error) {

	coll := pr.getCurrentCollection()

	result := &models.Post{}
	_id, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return nil, err
	}

	err = coll.FindOne(ctx, bson.D{{"_id", _id}}).Decode(result)

	if err != nil {
		return nil, errors.Wrap(err, "PostsMongoRepo.GetById.FindOne")
	}

	return result, nil
}

func (pr *postsRepository) GetAll(ctx context.Context) ([]models.Post, error) {

	coll := pr.getCurrentCollection()
	cur, err := coll.Find(ctx, bson.D{})

	if err != nil {
		return nil, errors.Wrap(err, "PostsMongoRepo.GetAll.Find")
	}

	var results = make([]models.Post, 0, 20)

	if err = cur.All(ctx, &results); err != nil {
		return nil, err
	}
	return results, nil
}

func (pr *postsRepository) GetByCategory(ctx context.Context, category string) ([]models.Post, error) {

	coll := pr.getCurrentCollection()

	cur, err := coll.Find(ctx, bson.M{"category": bson.M{"$eq": category}})

	if err != nil {
		if err == mongo.ErrNilDocument {
			return []models.Post{}, nil
		}
		return nil, errors.Wrap(err, "PostsMongoRepo.GetByCategory.Find")
	}

	var results = make([]models.Post, 0, 20)

	if err = cur.All(ctx, &results); err != nil {
		return nil, err
	}
	return results, nil

}

func (pr *postsRepository) Delete(ctx context.Context, postId string, username string) error {

	coll := pr.getCurrentCollection()

	_id, err := primitive.ObjectIDFromHex(postId)

	if err != nil {
		return err
	}

	res := coll.FindOneAndDelete(ctx, bson.D{{"_id", _id}, {"author_info.username", username}})

	if res.Err() != nil {
		return errors.Wrap(res.Err(), "PostsMongoRepo.Delete.FindOneAmdDelete")
	}

	return nil
}

func (pr *postsRepository) PostsByUsername(ctx context.Context, username string) ([]models.Post, error) {

	coll := pr.getCurrentCollection()

	cur, err := coll.Find(ctx, bson.M{"author_info.username": username})

	if err != nil {
		if err == mongo.ErrNilDocument {
			return []models.Post{}, nil
		}
		return nil, errors.Wrap(err, "PostsMongoRepo.PostsByUsername.Find")
	}

	var results = make([]models.Post, 0, 20)

	if err = cur.All(ctx, &results); err != nil {
		return nil, err
	}
	return results, nil

}

func (pr *postsRepository) Vote(ctx context.Context, postId string, userId int, voteVal int) (*models.Post, error) {

	_id, err := primitive.ObjectIDFromHex(postId)

	if err != nil {
		return nil, errors.Wrap(httpErrors.NewBadRequest(err), "postsRepository.Unvote.objectIDhex")
	}

	sessOps := options.Session().SetDefaultReadConcern(readconcern.Majority())
	sess, err := pr.mongoClient.StartSession(sessOps)
	if err != nil {
		return nil, errors.Wrap(err, "postsRepository.Unvote.StartSession")
	}
	defer sess.EndSession(ctx)

	txnOpts := options.Transaction().SetReadPreference(readpref.Primary())

	res, err := sess.WithTransaction(ctx, func(sessCtx mongo.SessionContext) (interface{}, error) {

		coll := pr.getCurrentCollection()
		filter := bson.M{"_id": _id, "votes.user_id": userId}
		p := &models.Post{}
		sessErr := coll.FindOne(sessCtx, filter).Decode(p)

		if sessErr == nil {
			for _, vote := range p.Votes {
				if vote.UserId == userId {
					p.Score -= vote.Value
					vote.Value = voteVal
					break
				}
			}
			p.Score += voteVal
			p.UpvotePercentage = (p.Score / len(p.Votes)) * 100
			_, sessErr = coll.ReplaceOne(ctx, filter, p)
			if sessErr != nil {
				return nil, sessErr
			}
			return p, nil
		}

		if sessErr == mongo.ErrNoDocuments {
			p, sessErr = pr.createNewVote(sessCtx, _id, userId, voteVal)
			if sessErr != nil {
				return nil, sessErr
			}
			match := bson.M{"_id": _id}
			p.Score += voteVal
			p.UpvotePercentage = (p.Score / len(p.Votes)) * 100
			//filter = bson.M{"$set": bson.M{"votes.$.value": -1}}
			_, sessErr = coll.ReplaceOne(sessCtx, match, p)
			if sessErr != nil {
				return nil, sessErr
			}
			return p, nil
		}

		return nil, sessErr
	}, txnOpts)

	if err != nil {
		return nil, err
	}

	p := res.(*models.Post)
	return p, nil
}

//func (pr *postsRepository) DownVote(ctx context.Context, postId string, userId int) (*models.Post, error) {
//	_id, err := primitive.ObjectIDFromHex(postId)
//
//	if err != nil {
//		return nil, errors.Wrap(httpErrors.NewBadRequest(err), "postsRepository.Unvote.objectIDhex")
//	}
//
//	coll := pr.getCurrentCollection()
//
//	ops := options.FindOneAndUpdate().SetUpsert(false)
//	ops.SetReturnDocument(options.After)
//	match := bson.M{"_id": _id, "votes.user_id": userId}
//	//, "votes": []bson.M{{"user_id": userId}}
//	filter := bson.M{"$set": bson.M{"votes.$.value": -1}}
//
//	p := &models.Post{}
//	err = coll.FindOneAndUpdate(ctx, match, filter, ops).Decode(p)
//	if err != nil {
//		if err == mongo.ErrNoDocuments {
//			//create new one
//			p, err = pr.createNewVote(ctx, _id, userId)
//
//			if err != nil {
//				return nil, err
//			}
//			return p, nil
//		}
//		return nil, errors.Wrap(err, "postsRepository.Unvote.findoneandupdate")
//	}
//
//	return p, nil
//}
//
//func (pr *postsRepository) UnVote(ctx context.Context, postId string, userId int) (*models.Post, error) {
//	_id, err := primitive.ObjectIDFromHex(postId)
//
//	if err != nil {
//		return nil, errors.Wrap(httpErrors.NewBadRequest(err), "postsRepository.Unvote.objectIDhex")
//	}
//
//	coll := pr.getCurrentCollection()
//
//	ops := options.FindOneAndUpdate().SetUpsert(false)
//	ops.SetReturnDocument(options.After)
//	match := bson.M{"_id": _id, "votes.user_id": userId}
//	//, "votes": []bson.M{{"user_id": userId}}
//	filter := bson.M{"$set": bson.M{"votes.$.value": 0}}
//
//	p := &models.Post{}
//	err = coll.FindOneAndUpdate(ctx, match, filter, ops).Decode(p)
//	if err != nil {
//		if err == mongo.ErrNoDocuments {
//			//create new one
//			p, err = pr.createNewVote(ctx, _id, userId)
//
//			if err != nil {
//				return nil, err
//			}
//			return p, nil
//		}
//		return nil, errors.Wrap(err, "postsRepository.Unvote.findoneandupdate")
//	}
//
//	return p, nil
//}

func (pr *postsRepository) getCurrentCollection() *mongo.Collection {
	return pr.mongoClient.Database(pr.cfc.Mongo.MongoDB).Collection(collectionName)
}

func (pr *postsRepository) createNewVote(ctx context.Context, id primitive.ObjectID, userId int, voteVal int) (*models.Post, error) {
	coll := pr.getCurrentCollection()
	ops := options.FindOneAndUpdate().SetUpsert(false)
	ops.SetReturnDocument(options.After)
	match := bson.M{"_id": id}
	//, "votes": []bson.M{{"user_id": userId}}
	filter := bson.M{"$push": bson.M{"votes": &models.Vote{
		UserId: userId,
		Value:  voteVal,
	}}}
	p := &models.Post{}
	err := coll.FindOneAndUpdate(ctx, match, filter, ops).Decode(p)

	if err != nil {
		return nil, err
	}
	return p, nil
}
