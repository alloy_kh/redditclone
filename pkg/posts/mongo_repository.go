package posts

import (
	"context"
	"gitlab.com/alloy_kh/redditclone/pkg/models"
)

type Repository interface {
	GetById(ctx context.Context, id string) (*models.Post, error)
	GetByCategory(ctx context.Context, category string) ([]models.Post, error)
	GetAll(ctx context.Context) ([]models.Post, error)
	Create(ctx context.Context, newPost *models.PostCreate) (*models.Post, error)
	Delete(ctx context.Context, postId string, username string) error
	PostsByUsername(ctx context.Context, username string) ([]models.Post, error)

	// could take voting to upper layer if you want
	Vote(ctx context.Context, postId string, userId int, voteVal int) (*models.Post, error)
}
