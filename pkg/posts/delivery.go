package posts

import "net/http"

type Handlers interface {
	Create(w http.ResponseWriter, r *http.Request)
	GetById(w http.ResponseWriter, r *http.Request)
	GetByCategory(w http.ResponseWriter, r *http.Request)
	GetAll(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	GetUserPosts(w http.ResponseWriter, r *http.Request)

	// could take voting to upper layer if you want
	UpVote(w http.ResponseWriter, r *http.Request)
	DownVote(w http.ResponseWriter, r *http.Request)
	UnVote(w http.ResponseWriter, r *http.Request)
}
