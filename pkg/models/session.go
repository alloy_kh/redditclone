package models

import "time"

type Session struct {
	SessionId string         `db:"session_id"`
	UserInfo  *UserShortInfo `db:"-"`
	CreatedAt time.Time      `db:"created_at"`
	Role      string         `db:"-"`
}
