package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Comment struct {
	ID        primitive.ObjectID `json:"id" bson:"_id"`
	PostId    primitive.ObjectID `json:"post_id" bson:"post_id"`
	Author    *UserShortInfo     `json:"author" bson:"author_info"`
	Body      string             `json:"body" bson:"body"`
	CreatedAt time.Time          `json:"created" bson:"created_at"`
}
