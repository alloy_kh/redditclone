package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Post struct {
	ID               primitive.ObjectID `bson:"_id"`
	Title            string             `bson:"title"`
	Score            int                `bson:"score"`
	Category         string             `bson:"category"`
	UpvotePercentage int                `bson:"upvote_percentage"`
	Views            int                `bson:"views"`
	CreatedAt        time.Time          `bson:"created_at"`
	Author           *UserShortInfo     `bson:"author_info"`
	Type             string             `bson:"type"`
	Url              string             `bson:"url"`

	Votes    []*Vote    `bson:"votes"`
	Comments []*Comment `bson:"comments"`
}

type PostCreate struct {
	Title            string         `json:"title" valid:"required" bson:"title"`
	Category         string         `json:"category" valid:"required" bson:"category"`
	Type             string         `json:"type" valid:"required" bson:"type"`
	Url              string         `json:"url" valid:"required" bson:"Url"`
	Score            int            `json:"-" bson:"score"`
	UpvotePercentage int            `json:"-" bson:"upvote_percentage"`
	Views            int            `json:"-" bson:"views"`
	CreatedAt        time.Time      `json:"-" bson:"created_at"`
	Author           *UserShortInfo `json:"-" bson:"author_info"`
	Votes            []*Vote        `json:"-" bson:"votes"`
	Comments         []*Comment     `json:"-" bson:"comments"`
}

func (p *PostCreate) PreparePostToSave(usr *UserShortInfo) {
	p.CreatedAt = time.Now()
	p.Author = usr
	p.Votes = []*Vote{}
	p.Comments = []*Comment{}
}

//type Post struct {
//	ID               string
//	Author           *User
//	Category         string
//	Comments         []*Comment
//	Created          string
//	Score            int
//	Title            string
//	UpvotePercentage uint8
//	Views            uint32
//	Votes            []*Vote
//	VotesMap         map[string]*Vote `json:"-"`
//}
//
