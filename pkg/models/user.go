package models

import (
	"golang.org/x/crypto/bcrypt"
	"strings"
	"time"
)

type User struct {
	ID        int
	Email     string
	Password  string `json:"-"`
	Role      string
	CreatedAt time.Time `json:"created_at,omitempty" db:"created_at"`
	UpdatedAt time.Time `json:"updated_at,omitempty" db:"updated_at"`
	LoginDate time.Time `json:"login_date" db:"login_date"`
}

type Login struct {
	Email    string `json:"email" db:"email" valid:"required,email"`
	Password string `json:"password,omitempty" db:"password" valid:"required"`
}

type UserWithToken struct {
	User  *User
	Token string
}

type JustToken struct {
	Token string
}

type UserCtxInfo struct {
	ID       int
	Role     string
	Username string
}

type UserShortInfo struct {
	ID       int    `json:"id" bson:"id"`
	Username string `json:"username" bson:"username"`
}

func (u *User) GetShortUserInfo() *UserShortInfo {
	return &UserShortInfo{
		ID:       u.ID,
		Username: u.Email,
	}
}

func (u *User) CheckPassword(psw string) error {
	return bcrypt.CompareHashAndPassword([]byte(psw), []byte(u.Password))
}

func (u *User) SanitizePassword() {
	u.Password = ""
}

func (u *User) HashPassword() error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)
	return nil
}

func (u *User) PrepareToSave() error {

	u.Email = strings.ToLower(strings.TrimSpace(u.Email))
	u.Password = strings.TrimSpace(u.Password)

	if err := u.HashPassword(); err != nil {
		return err
	}

	return nil
}
