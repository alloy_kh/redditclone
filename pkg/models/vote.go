package models

type Vote struct {
	UserId int `json:"user_id" bson:"user_id"`
	Value  int `json:"value" bson:"value"`
}
